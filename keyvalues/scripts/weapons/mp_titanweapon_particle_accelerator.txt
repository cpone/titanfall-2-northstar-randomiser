WeaponData
{
	"OnWeaponPrimaryAttack"							"OnWeaponPrimaryAttack_randomiser_smart_core_particle_accelerator"

	"smart_ammo_search_angle" 						"60"
	"smart_ammo_search_distance"  					"3000"
	"smart_ammo_search_npcs"						"1"
	"smart_ammo_search_players"						"1"
	"smart_ammo_search_projectiles"					"0"
	"smart_ammo_titans_block_los"					"1"
	"smart_ammo_new_target_delay"					"0"
	"smart_ammo_max_targets"						"6"
	"smart_ammo_alt_lock_style"						"0"
	"smart_ammo_max_targeted_burst"					"12"
	"smart_ammo_always_do_burst"					"0"
	"smart_ammo_targeting_time_min"					"0.5"
	"smart_ammo_targeting_time_max"					"0.5"
	"smart_ammo_target_max_locks_heavy"				"1"
	"smart_ammo_hud_lock_style"						"default"
	"smart_ammo_hud_type"							"predator_cannon"
	"smart_ammo_target_confirmed_sound"				"Titan_Legion_Smart_Core_Target_Acquired_1P"
	"smart_ammo_target_confirming_sound"			"Titan_Legion_Smart_Core_LockingOn_1P"
	"smart_ammo_target_found_sound"					"Titan_Legion_Smart_Core_LockingOn_1P"
	"smart_ammo_target_lost_sound"					"Titan_Legion_Smart_Core_Unlocked_1P"
	"smart_ammo_lock_type"							""
	"smart_ammo_allow_ads_lock"						"0"
	"smart_ammo_allow_hip_fire_lock"				"0"
	"smart_ammo_draw_acquisition_lines"				"0"
	"smart_ammo_titan_lock_point0"					"SMART_AMMO_TORSO_FRONT"
	"smart_ammo_titan_lock_point1"					"SMART_AMMO_TORSO_BACK"
	"smart_ammo_titan_lock_point2"					"SMART_AMMO_LEG_LEFT"
	"smart_ammo_titan_lock_point3"					"SMART_AMMO_LEG_RIGHT"
	"smart_ammo_titan_lock_point4"					"SMART_AMMO_TORSO_BASE"
	"smart_ammo_titan_lock_point5"					"SMART_AMMO_HEAD"
	"smart_ammo_titan_lock_point6"					"SMART_AMMO_ARM_LEFT"
	"smart_ammo_titan_lock_point7"					"SMART_AMMO_ARM_RIGHT"
	"smart_ammo_points_search_tick_interval"		"6"
	"smart_ammo_bounds_search_tick_interval"		"6"

	Mods
	{
		arc_rounds
		{
			"fx_muzzle_flash_view"							"wpn_muzzleflash_xo_elec_FP"
			"fx_muzzle_flash_world"							"wpn_muzzleflash_xo_elec"

			"tracer_effect"   								"P_wpn_tracer_xo16_elec"
			"tracer_effect_first_person"  					"P_wpn_tracer_xo16_elec"
			"impact_effect_table"		 					"titan_bullet_elec"

			"vortex_drain"									"0.033"

			"ammo_clip_size"   								"*1.25"
			"ammo_clip_reload_max"							"*1.25"

			"damage_flags"									"DF_STOPS_TITAN_REGEN | DF_BULLET | DF_NO_SELF_DAMAGE | DF_ELECTRICAL"
		}
		rapid_reload
		{
			"reload_time" 									"*0.75"
			"reload_time_late1"								"*0.75"
			"reload_time_late2"								"*0.75"
			"reload_time_late3"								"*0.75"
			"reloadempty_time"								"*0.75"
			"reloadempty_time_late1"						"*0.75"
			"reloadempty_time_late2"						"*0.75"
		}
		Smart_Core
		{
			"smart_ammo_allow_ads_lock"						"1"
			"smart_ammo_allow_hip_fire_lock"				"1"
			"smart_ammo_allow_search_while_firing"			"1"
			"ammo_no_remove_from_clip"						"1"
			"vortex_drain"									"0.033"

			"aimassist_disable_hipfire"						"1"
			"aimassist_disable_ads"							"1"

			"smart_ammo_lock_type"							"any"
		}
	}
}